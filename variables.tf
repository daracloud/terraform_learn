variable vpc_cidr_block {
     default = "10.0.0.0/16"
}

variable subnet_cidr_block {
    default = "10.0.10.0/24"
}

variable avail_zone {
default = "us-east-2a"
} 

variable env_prefix {
    default = "dev"
}

variable instance_type {
default = "t2.micro"
}


variable region {
    default = "us-east-2"
}

variable public_key_location {
    default = "/home/ec2-user/.ssh/id_rsa.pub"
}

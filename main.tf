provider "aws" {
  region  = "us-east-2"
  profile = "default"
}



#Create a virtual network
resource "aws_vpc" "my_vpc" {
    cidr_block = var.vpc_cidr_block    
    tags = {
        Name = "${var.env_prefix}-vpc"
  }
}
#Create your subnet
resource "aws_subnet" "my_app-subnet" {
  
    vpc_id = aws_vpc.my_vpc.id
    cidr_block =  var.subnet_cidr_block  
    availability_zone = var.avail_zone
    map_public_ip_on_launch = true
   depends_on= [aws_vpc.my_vpc]

     tags = {
        Name = "${var.env_prefix}-subnet-1"
    }
 }
   #create your route table
   resource "aws_route_table" "myapp-route-table" {
     vpc_id = aws_vpc.my_vpc.id

    route {
        cidr_block = "0.0.0.0/0"
        gateway_id = aws_internet_gateway.myapp-igw.id
        
    }
   }
   
# create internet gateway
   resource "aws_internet_gateway" "myapp-igw" {
       vpc_id = aws_vpc.my_vpc.id
       tags = {
         Name: "${var.env_prefix}-igw"
        }
  }


resource "aws_route_table_association" "a-rtb-subnet" {
    subnet_id = aws_subnet.my_app-subnet.id
    route_table_id = aws_route_table.myapp-route-table.id

  }
#create default route table
   resource "aws_default_route_table" "main-rtb" {
     default_route_table_id = aws_vpc.my_vpc.default_route_table_id

route {
  cidr_block = "0.0.0.0/0"
  gateway_id = aws_internet_gateway.myapp-igw.id
}
tags = {
  Name: "${var.env_prefix}-main-rtb"
}
   }

   #Create a security group
resource "aws_security_group" "App_SG" {
    name = "App_SG"
    description = "Allow Web inbound traffic"
    vpc_id = aws_vpc.my_vpc.id
    ingress  {
        protocol = "tcp"
        from_port = 80
        to_port  = 80
        cidr_blocks = ["0.0.0.0/0"]
    }
    ingress  {
        protocol = "tcp"
        from_port = 22
        to_port  = 22
        cidr_blocks = ["0.0.0.0/0"]
    }
    egress  {
        protocol = "-1"
        from_port = 0
        to_port  = 0
        cidr_blocks = ["0.0.0.0/0"]



    }
    tags = {
         Name: "${var.env_prefix}-App_SG"
}
}

resource "aws_instance" "webserver" {
   ami = data.aws_ami.latest-amazon-linux-image.id
   instance_type = var.instance_type

   subnet_id = aws_subnet.my_app-subnet.id
   vpc_security_group_ids = [aws_security_group.App_SG.id]
   availability_zone = var.avail_zone

   user_data = file("entry-script.sh")

   tags = {
         Name: "${var.env_prefix}-webserver"

associate_public_ip_address = true
key_name = aws_key_pair.ssh-key.key_name

}
}

    data "aws_ami" "latest-amazon-linux-image" {
    most_recent = true
    owners = ["amazon"]

    filter {
        name = "name"
        values = ["amzn2-ami-hvm-*-x86_64-gp2"]
    }

    filter {
        name = "virtualization-type"
        values = ["hvm"]
    }
 }
output "aws_ami_id" {
    value = data.aws_ami.latest-amazon-linux-image.id
}


resource "aws_key_pair"  "ssh-key" {
  key_name = "server-key"
  public_key = file(var.public_key_location)
}
   
